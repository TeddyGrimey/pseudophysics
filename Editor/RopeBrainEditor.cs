﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace PseudoPhysics
{
    [CustomEditor(typeof(RopeBrain))]
    [CanEditMultipleObjects] 
    public class RopeBrainEditor : Editor
    {
        private void OnSceneGUI()
        {
            var ropeBrain = (RopeBrain) target;
            for (int i = 0; i < ropeBrain.Constraints.Length - 1; i++)
            {
                for (int o = 0; o < ropeBrain.Constraints[i].Offsets.Count; o++)
                {
                    ropeBrain.Constraints[i].Offsets[o] = Handles.PositionHandle(ropeBrain.Constraints[i].Offsets[o], Quaternion.identity);
                }
            }
        }
    }
}
