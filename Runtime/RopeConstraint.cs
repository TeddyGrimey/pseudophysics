﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PseudoPhysics
{
    [System.Serializable]
    public class RopeConstraint
    {
        public Transform Transform;
        public float OffsetTime = 0.5f;
        public List<Vector3> Offsets = new List<Vector3>();
        public float OffsetWidth;
        public bool Static = true;
        public float Mass = 1;
    }
}
