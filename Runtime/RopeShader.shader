﻿Shader "ICE/Special/Rope"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Tags {"LightMode"="ForwardBase"}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma target 4.0 

            #include "UnityCG.cginc"

            float4 _MassArray[2000];
            float _Radius;
            float4 _Color;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1) 
                float4 vertex : SV_POSITION;
                float3 centre : TEXCOORD1;
                float3 worldPos : TEXCOORD2;
                float3 normal : NORMAL;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float is_equal(float x, float y) {
              return 1.0 - abs(sign(x - y));
            }

            v2f vert (appdata v, uint vid : SV_VertexID)
            {
                v2f o;

                uint t_idx = vid / 12; 
                uint v_idx = vid - t_idx * 12;

                fixed3 viewDir0 = normalize(_WorldSpaceCameraPos - _MassArray[t_idx]);
                fixed3 viewDir1 = normalize(_WorldSpaceCameraPos - _MassArray[t_idx + 1]);

                float3 forwardVector0 = normalize(_MassArray[t_idx + 1] - _MassArray[t_idx]);
                float3 forwardVector1 = normalize(_MassArray[t_idx + 2] - _MassArray[t_idx + 1]);

                float3 sideVector0 = normalize( cross(forwardVector0, viewDir0));
                float3 sideVector1 = normalize( cross(forwardVector1, viewDir1));

                float3 upVector0 = cross(forwardVector0, sideVector0);
                float3 upVector1 = cross(forwardVector1, sideVector1);

                float3 v1 = _MassArray[t_idx] - (sideVector0 * _Radius * 0.5) + (upVector0 * _Radius * 0.5);
                float3 v2 = _MassArray[t_idx] + (sideVector0 * _Radius * 0.5) + (upVector0 * _Radius * 0.5);
                float3 v3 = _MassArray[t_idx] + (sideVector0 * _Radius * 0.5) - (upVector0 * _Radius * 0.5);
                float3 v4 = _MassArray[t_idx] - (sideVector0 * _Radius * 0.5) - (upVector0 * _Radius * 0.5);

                float3 v5 = _MassArray[t_idx + 1] - (sideVector1 * _Radius * 0.5) + (upVector1 * _Radius * 0.5);
                float3 v6 = _MassArray[t_idx + 1] + (sideVector1 * _Radius * 0.5) + (upVector1 * _Radius * 0.5);
                float3 v7 = _MassArray[t_idx + 1] + (sideVector1 * _Radius * 0.5) - (upVector1 * _Radius * 0.5);
                float3 v8 = _MassArray[t_idx + 1] - (sideVector1 * _Radius * 0.5) - (upVector1 * _Radius * 0.5);
                
        
                if((vid / 3) % 2 == 0)
                {
                    v.vertex.xyz = lerp(lerp(v5,v6,is_equal(v_idx, 1)),v1,is_equal(v_idx, 0));
                    o.uv = lerp(lerp(float2(0,1),float2(1,1),is_equal(v_idx, 1)),float2(0,0),is_equal(v_idx, 0));
                }
                else if((vid / 3) % 2 == 1)
                {
                    v.vertex.xyz = lerp(lerp(v2,v1,is_equal(v_idx, 4)),v6,is_equal(v_idx, 3));
                    o.uv = lerp(lerp(float2(1,0),float2(0,0),is_equal(v_idx, 4)),float2(1,1),is_equal(v_idx, 3));
                }
                if(is_equal(v_idx, 0) || is_equal(v_idx, 4) || is_equal(v_idx, 5))
                {
                    o.centre = _MassArray[t_idx];
                    o.normal = upVector0;
                }
                else
                {
                    o.centre = _MassArray[t_idx + 1];
                    o.normal = upVector1;
                }
                

                
                
                o.worldPos = mul (unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;

                float3 normal = normalize((i.worldPos + (-i.normal * _Radius)) - i.centre);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

                col.rgb *= dot(lightDirection, normal);
                //col.rgb  = normal;
                return col;
            }
            ENDCG
        }
        Pass
        {
            Name "ShadowCaster"
            Tags { "LightMode" = "ShadowCaster" }
            ZWrite On ZTest LEqual Cull Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #pragma multi_compile_shadowcaster 
            #pragma target 4.0 

            #include "UnityCG.cginc"

            float4 _MassArray[2000];
            float _Radius;
            float4 _Color;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1) 
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float is_equal(float x, float y) {
              return 1.0 - abs(sign(x - y));
            }

            v2f vert (appdata v, uint vid : SV_VertexID)
            {
                v2f o;

                uint t_idx = vid / 12; 
                uint v_idx = vid - t_idx * 12;

                fixed3 viewDir0 = normalize(_WorldSpaceLightPos0.xyz - _MassArray[t_idx]);
                fixed3 viewDir1 = normalize(_WorldSpaceLightPos0.xyz - _MassArray[t_idx + 1]);

                float3 forwardVector0 = normalize(_MassArray[t_idx + 1] - _MassArray[t_idx]);
                float3 forwardVector1 = normalize(_MassArray[t_idx + 2] - _MassArray[t_idx + 1]);

                float3 sideVector0 = normalize( cross(forwardVector0, viewDir0));
                float3 sideVector1 = normalize( cross(forwardVector1, viewDir1));

                float3 upVector0 = float3(0,0,0); //cross(forwardVector0, sideVector0);
                float3 upVector1 = float3(0,0,0); //cross(forwardVector1, sideVector1);

                float3 v1 = _MassArray[t_idx] - (sideVector0 * _Radius * 0.5) + (upVector0 * _Radius * 0.5);
                float3 v2 = _MassArray[t_idx] + (sideVector0 * _Radius * 0.5) + (upVector0 * _Radius * 0.5);
                float3 v3 = _MassArray[t_idx] + (sideVector0 * _Radius * 0.5) - (upVector0 * _Radius * 0.5);
                float3 v4 = _MassArray[t_idx] - (sideVector0 * _Radius * 0.5) - (upVector0 * _Radius * 0.5);

                float3 v5 = _MassArray[t_idx + 1] - (sideVector1 * _Radius * 0.5) + (upVector1 * _Radius * 0.5);
                float3 v6 = _MassArray[t_idx + 1] + (sideVector1 * _Radius * 0.5) + (upVector1 * _Radius * 0.5);
                float3 v7 = _MassArray[t_idx + 1] + (sideVector1 * _Radius * 0.5) - (upVector1 * _Radius * 0.5);
                float3 v8 = _MassArray[t_idx + 1] - (sideVector1 * _Radius * 0.5) - (upVector1 * _Radius * 0.5);
                
        
                if((vid / 3) % 2 == 0)
                {
                    v.vertex.xyz = lerp(lerp(v5,v6,is_equal(v_idx, 1)),v1,is_equal(v_idx, 0));
                }
                else if((vid / 3) % 2 == 1)
                {
                    v.vertex.xyz = lerp(lerp(v2,v1,is_equal(v_idx, 4)),v6,is_equal(v_idx, 3));
                }
                

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return 0;
            }
            ENDCG
        }
    }
}
