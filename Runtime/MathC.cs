﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathC
{

    private static Vector2 pointOne;
    private static Vector2 pointTwo;
    private static Vector2 pointOneVector;
    private static Vector2 vectorToPoint;
    private static Vector2 reflectedVector;
    private static Vector2 midPoint;

    public static Vector3 FindPointOnArc(out CurveSegmentData a_trackdata, Vector3 a_pointOne, Vector3 a_pointOneVector, Vector3 a_pointTwo, float a_t, float a_offset = 0)
    {
        var newPoint = Vector2.zero;

        pointOne = new Vector2(a_pointOne.x, a_pointOne.z);
        pointTwo = new Vector2(a_pointTwo.x, a_pointTwo.z);
        pointOneVector = new Vector2(a_pointOneVector.x, a_pointOneVector.z);

        var angleToPointTwo = AngleHalf(pointOneVector, (pointTwo - pointOne).normalized, Vector3.back);

        if (angleToPointTwo == 0)
        {
            pointOneVector = RotateVectorByDeg(pointOneVector, 0.1f).normalized;
        }
        reflectedVector = RotateVectorByDeg(pointOneVector, -90);
        if (angleToPointTwo < 0)
        {
            reflectedVector = RotateVectorByDeg(pointOneVector, 90);
        }
        vectorToPoint = (pointTwo - pointOne).normalized;

        midPoint = Vector2.Lerp(pointOne, pointTwo, 0.5f);

        var toCentreLength = Mathf.Tan(AngleHalf(reflectedVector, vectorToPoint, Vector3.forward) * Mathf.Deg2Rad) * Vector2.Distance(pointOne, midPoint);

        var midPointVector = -RotateVectorByDeg(vectorToPoint, 90);
        if (angleToPointTwo < 0)
        {
            midPointVector = RotateVectorByDeg(vectorToPoint, 90);
        }

        var centrePoint = midPoint + (midPointVector * toCentreLength);


        var radius = Vector2.Distance(centrePoint, pointOne);
        var angleBetweenPoints = AngleFull((pointOne - centrePoint).normalized, (pointTwo - centrePoint).normalized, Vector3.back);

        if (angleToPointTwo < 0)
        {
            centrePoint = midPoint - (midPointVector * toCentreLength);
            angleBetweenPoints = AngleFull((pointTwo - centrePoint).normalized, (pointOne - centrePoint).normalized, Vector3.back);
        }

        var angleNormalised = Mathf.Lerp(0, -angleBetweenPoints, a_t);

        a_trackdata = new CurveSegmentData(a_pointOne, a_pointOneVector, a_pointTwo);


        if (angleToPointTwo < 0)
        {
            newPoint = centrePoint + (RotateVectorByDeg((pointOne - centrePoint).normalized, -angleNormalised) * (radius - a_offset));
        }
        else
        {
            newPoint = centrePoint + (RotateVectorByDeg((pointOne - centrePoint).normalized, angleNormalised) * (radius + a_offset));
        }

        var newPoint3D = new Vector3(newPoint.x, 0, newPoint.y);


        var endPointVector = Vector3.zero;
        if (angleToPointTwo < 0)
        {
            endPointVector.x = RotateVectorByDeg((centrePoint - pointTwo).normalized, 90).x;
            endPointVector.z = RotateVectorByDeg((centrePoint - pointTwo).normalized, 90).y;
        }
        else
        {
            endPointVector.x = RotateVectorByDeg((centrePoint - pointTwo).normalized, -90).x;
            endPointVector.z = RotateVectorByDeg((centrePoint - pointTwo).normalized, -90).y;
        }

        //Debug.DrawRay(a_pointTwo, endPointVector);
        a_trackdata.m_vectorOne = new Vector3(pointOneVector.x, a_pointOneVector.y, pointOneVector.y);
        a_trackdata.m_vectorTwo = endPointVector;
        a_trackdata.m_centrePoint = new Vector3(centrePoint.x, 0, centrePoint.y);
        a_trackdata.m_radius = radius;

        a_trackdata.m_angleToPointTwo = angleToPointTwo;
        if (angleToPointTwo < 0)
        {
            a_trackdata.m_angleToTwoFromCentre = AngleFull((pointOne - centrePoint).normalized, (pointTwo - centrePoint).normalized, Vector3.forward);
            a_trackdata.m_inner = true;
        }
        else
        {
            a_trackdata.m_angleToTwoFromCentre = AngleFull((pointOne - centrePoint).normalized, (pointTwo - centrePoint).normalized, Vector3.back);
            a_trackdata.m_inner = false;
        }

        a_trackdata.m_arcLength = 2 * Mathf.PI * a_trackdata.m_radius * (Mathf.Deg2Rad * a_trackdata.m_angleToTwoFromCentre);
        return newPoint3D;
    }

    public static Vector3 FindPointOnArc(Vector3 a_pointOne, Vector3 a_pointOneVector, Vector3 a_pointTwo, float a_t, float a_offset = 0)
    {
        CurveSegmentData trackData;
        return FindPointOnArc(out trackData, a_pointOne, a_pointOneVector, a_pointTwo, a_t, a_offset);
    }



    public static Vector3 FindPointOnArc(CurveSegmentData a_trackData, float a_t, float a_offset = 0)
    {
        var newPoint = Vector2.zero;

        pointOne = new Vector2(a_trackData.m_pointOne.x, a_trackData.m_pointOne.z);
        pointTwo = new Vector2(a_trackData.m_pointTwo.x, a_trackData.m_pointTwo.z);
        pointOneVector = new Vector2(a_trackData.m_vectorOne.x, a_trackData.m_vectorOne.z);

        /*if (a_trackData.m_angleToPointTwo == 0)
        {
            pointOneVector = RotateVectorByDeg(pointOneVector, 0.1f).normalized;
        }*/

        reflectedVector = RotateVectorByDeg(pointOneVector, -90);
        if (a_trackData.m_angleToPointTwo < 0)
        {
            reflectedVector = RotateVectorByDeg(pointOneVector, 90);
        }
        vectorToPoint = (pointTwo - pointOne).normalized;

        midPoint = Vector2.Lerp(pointOne, pointTwo, 0.5f);

        var toCentreLength = Mathf.Tan(AngleHalf(reflectedVector, vectorToPoint, Vector3.forward) * Mathf.Deg2Rad) * Vector2.Distance(pointOne, midPoint);

        var midPointVector = -RotateVectorByDeg(vectorToPoint, 90);
        var centrePoint = new Vector2(a_trackData.m_centrePoint.x, a_trackData.m_centrePoint.z);

        if (a_trackData.m_angleToPointTwo < 0)
        {
            midPointVector = RotateVectorByDeg(vectorToPoint, 90);
            centrePoint = midPoint - (midPointVector * toCentreLength);
        }

        var radius = Vector2.Distance(centrePoint, pointOne);
        var angleBetweenPoints = AngleFull((pointOne - centrePoint).normalized, (pointTwo - centrePoint).normalized, Vector3.back);

        if (a_trackData.m_angleToPointTwo < 0)
        {
            angleBetweenPoints = AngleFull((pointTwo - centrePoint).normalized, (pointOne - centrePoint).normalized, Vector3.back);
        }

        var angleNormalised = Mathf.Lerp(0, -angleBetweenPoints, a_t);


        if (a_trackData.m_angleToPointTwo < 0)
        {
            newPoint = centrePoint + (RotateVectorByDeg((pointOne - centrePoint).normalized, -angleNormalised) * (radius - a_offset));
        }
        else
        {
            newPoint = centrePoint + (RotateVectorByDeg((pointOne - centrePoint).normalized, angleNormalised) * (radius + a_offset));
        }

        var newPoint3D = new Vector3(newPoint.x, 0, newPoint.y);

        return newPoint3D;
    }

    public static Vector3 FindPointOnArc(out CurveSegmentData a_pathData, CurveSegmentData a_trackData, float a_t, float a_offset = 0)
    {
        return FindPointOnArc(out a_pathData, a_trackData.m_pointOne, a_trackData.m_vectorOne, a_trackData.m_pointTwo, a_t, a_offset);
    }

    public static Vector3 FindBiarcPoint(Vector3 a_pointOne, Vector3 a_pointOneVector, Vector3 a_pointTwo, Vector3 a_pointTwoVector, float a_time = 0.5f)
    {
        Vector3 newPoint = Vector3.zero;

        if (a_pointOneVector == a_pointTwoVector)
        {
            a_pointTwoVector = RotateVectorByDegVector3(a_pointTwoVector, 0.1f);
        }

        float combinedAngle = 0;
        combinedAngle += (AngleHalf(Vector3.forward, a_pointOneVector, Vector3.up) + -AngleHalf(Vector3.forward, a_pointTwoVector, Vector3.up)) / 2;
        combinedAngle += AngleHalf(Vector3.forward, Vector3.Normalize(a_pointTwo - a_pointOne), Vector3.up);

        //DebugWrapper.Log(combinedAngle);

        Vector3 combined = Vector3.Normalize(Quaternion.AngleAxis(combinedAngle, Vector3.up) * Vector3.forward);

        CurveSegmentData trackData;

        Vector3 arcMid = Vector3.zero;

        arcMid = FindPointOnArc(out trackData, a_pointOne, combined, a_pointTwo, a_time);
        float length1 = 0;
        FindPointOnArc(out trackData, a_pointOne, a_pointOneVector, arcMid, 0);
        length1 += trackData.m_arcLength;
        FindPointOnArc(out trackData, a_pointTwo, -a_pointTwoVector, arcMid, 0);
        length1 += trackData.m_arcLength;

        arcMid = FindPointOnArc(out trackData, a_pointOne, -combined, a_pointTwo, a_time);
        float length2 = 0;
        FindPointOnArc(out trackData, a_pointOne, a_pointOneVector, arcMid, 0);
        length2 += trackData.m_arcLength;
        FindPointOnArc(out trackData, a_pointTwo, -a_pointTwoVector, arcMid, 0);
        length2 += trackData.m_arcLength;


        if (length2 > length1)
        {
            newPoint = FindPointOnArc(out trackData, a_pointOne, combined, a_pointTwo, a_time);
        }
        else
        {
            newPoint = FindPointOnArc(out trackData, a_pointOne, -combined, a_pointTwo, a_time);
        }

        newPoint.y = Mathf.Lerp(a_pointOne.y, a_pointTwo.y, 0.5f);

        return newPoint;
    }

    public static Vector2 RotateVectorByDeg(Vector2 a_vector, float a_deg)
    {
        float angleRad = a_deg * Mathf.Deg2Rad;
        Vector2 newVector = Vector2.zero;
        newVector.x = a_vector.x * Mathf.Cos(angleRad) - a_vector.y * Mathf.Sin(angleRad);
        newVector.y = a_vector.y * Mathf.Cos(angleRad) + a_vector.x * Mathf.Sin(angleRad);
        return newVector;
    }

    public static Vector3 RotateVectorByDegVector3(Vector3 a_vector, float a_deg)
    {
        float angleRad = a_deg * Mathf.Deg2Rad;
        Vector3 newVector = Vector3.zero;
        newVector.x = a_vector.x * Mathf.Cos(angleRad) - a_vector.z * Mathf.Sin(angleRad);
        newVector.z = a_vector.z * Mathf.Cos(angleRad) + a_vector.x * Mathf.Sin(angleRad);
        return newVector;
    }

    public static float AngleHalf(Vector3 v1, Vector3 v2, Vector3 n)
    {
        float toReturn = 0;
        toReturn = Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;

        return toReturn;
    }

    public static float Angle180(Vector3 v1, Vector3 v2, Vector3 n)
    {
        float toReturn = 0;
        toReturn = Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;

        return toReturn;
    }

    public static float AngleFull(Vector3 v1, Vector3 v2, Vector3 n)
    {
        float toReturn = 0;
        toReturn = Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        if (toReturn < 0)
            toReturn = 180 + (180 - -toReturn);

        return toReturn;
    }

    public static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle)
    {
        return angle * (point - pivot) + pivot;
    }

    public static Vector2 GetClosestPointOnLineSegment(Vector2 A, Vector2 B, Vector2 P)
    {
        Vector2 AP = P - A;       //Vector from A to P   
        Vector2 AB = B - A;       //Vector from A to B  

        float magnitudeAB = AB.SqrMagnitude();     //Magnitude of AB vector (it's length squared)     
        float ABAPproduct = Vector2.Dot(AP, AB);    //The DOT product of a_to_p and a_to_b     
        float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

        if (distance < 0)     //Check if P projection is over vectorAB     
        {
            return A;

        }
        else if (distance > 1)
        {
            return B;
        }
        else
        {
            return A + AB * distance;
        }
    }

    public static float SignedDistancePlanePoint(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
    {
        return Vector3.Dot(planeNormal, (point - planePoint));
    }

    public static Vector2 GetClosestPointOnRay(Vector2 A, Vector2 B, Vector2 P)
    {
        Vector2 AP = P - A;       //Vector from A to P   
        Vector2 AB = B - A;       //Vector from A to B  

        float magnitudeAB = AB.SqrMagnitude();     //Magnitude of AB vector (it's length squared)     
        float ABAPproduct = Vector2.Dot(AP, AB);    //The DOT product of a_to_p and a_to_b     
        float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

        return A + AB * distance;
    }

    public static Vector3 GetClosestPointOnLineSegment(Vector3 A, Vector3 B, Vector3 P)
    {
        Vector3 AP = P - A;       //Vector from A to P   
        Vector3 AB = B - A;       //Vector from A to B  

        float magnitudeAB = AB.sqrMagnitude;     //Magnitude of AB vector (it's length squared)     
        float ABAPproduct = Vector2.Dot(AP, AB);    //The DOT product of a_to_p and a_to_b     
        float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

        if (distance < 0)     //Check if P projection is over vectorAB     
        {
            return A;

        }
        else if (distance > 1)
        {
            return B;
        }
        else
        {
            return A + AB * distance;
        }
    }

    public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
    {
        Vector3 AB = b - a;
        Vector3 AV = value - a;
        return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
    }

    public static Vector3 CalculateCubicBezierPoint(Vector3 a_p0, Vector3 a_p1, Vector3 a_p2, Vector3 a_p3, float a_time)
    {
        float u = 1 - a_time;
        float tt = a_time * a_time;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * a_time;

        Vector3 p = uuu * a_p0;
        p += 3 * uu * a_time * a_p1;
        p += 3 * u * tt * a_p2;
        p += ttt * a_p3;

        return p;
    }

    public static float InverseLerpUnclamped(float from, float to, float value)
    {
        return (value - from) / (to - from);
    }

    public static bool GetCirclIntersectionPoints(Vector2 p1, float r1, Vector2 p2, float r2, out Vector2 i1, out Vector2 i2)
    {
        var dist = Vector2.Distance(p1, p2);
        if (dist > r1 + r2 || dist < Mathf.Abs(r1 - r2) || dist == 0 && r1 == r2)
        {
            i1 = Vector2.zero;
            i2 = Vector2.zero;
            return false;
        }
        else
        {
            var a = (r1 * r1 - r2 * r2 + dist * dist) / (2 * dist);
            var h = Mathf.Sqrt(r1 * r1 - a * a);
            var p3 = new Vector2(p1.x + a * (p2.x - p1.x) / dist, p1.y + a * (p2.y - p1.y) / dist);

            i1 = new Vector2(p3.x + h * (p2.y - p1.y) / dist, p3.y - h * (p2.x - p1.x) / dist);
            i2 = new Vector2(p3.x - h * (p2.y - p1.y) / dist, p3.y + h * (p2.x - p1.x) / dist);

            return true;
        }
    }

    public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
    {

        Vector3 lineVec3 = linePoint2 - linePoint1;
        Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
        Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

        float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

        //is coplanar, and not parrallel
        if (Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
        {
            float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
            intersection = linePoint1 + (lineVec1 * s);
            return true;
        }
        else
        {
            intersection = Vector3.zero;
            return false;
        }
    }

    public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
    {

        closestPointLine1 = Vector3.zero;
        closestPointLine2 = Vector3.zero;

        float a = Vector3.Dot(lineVec1, lineVec1);
        float b = Vector3.Dot(lineVec1, lineVec2);
        float e = Vector3.Dot(lineVec2, lineVec2);

        float d = a * e - b * b;

        //lines are not parallel
        if (d != 0.0f)
        {

            Vector3 r = linePoint1 - linePoint2;
            float c = Vector3.Dot(lineVec1, r);
            float f = Vector3.Dot(lineVec2, r);

            float s = (b * f - c * e) / d;
            float t = (a * f - c * b) / d;

            closestPointLine1 = linePoint1 + lineVec1 * s;
            closestPointLine2 = linePoint2 + lineVec2 * t;

            return true;
        }

        else
        {
            return false;
        }
    }

    public static float ClampMagnitude(float value, float magnitude)
    {
        magnitude = Mathf.Abs(magnitude);
        if (value > 0 && value > magnitude)
            return magnitude;
        else if (value < 0 && value < -magnitude)
            return -magnitude;

        return value;
    }

}

[System.Serializable]
public class CurveSegmentData
{
    public Vector3 m_pointOne;
    public Vector3 m_vectorOne;
    public Vector3 m_pointTwo;
    public Vector3 m_vectorTwo;
    public Vector3 m_centrePoint;
    public float m_angleToPointTwo;
    public float m_angleToTwoFromCentre;
    public float m_radius;
    public float m_arcLength;
    public bool m_inner;

    public CurveSegmentData()
    {

    }

    public CurveSegmentData(Vector3 a_pointOne, Vector3 a_vectorOne, Vector3 a_pointTwo)
    {
        m_pointOne = a_pointOne;
        m_vectorOne = a_vectorOne;
        m_pointTwo = a_pointTwo;
    }

    public Vector3 GetPositionOnCurve(float time)
    {
        var returnPosition = MathC.FindPointOnArc(this, time);
        returnPosition.y += MathC.CalculateCubicBezierPoint(m_pointOne, m_pointOne + (m_vectorOne * 2), m_pointTwo - (m_vectorTwo * 2), m_pointTwo, time).y;
        return returnPosition;
    }
    public Vector3 GetNormalOnCurve(float a_time)
    {
        var pointOnCurve = GetPositionOnCurve(a_time);
        return Vector3.Cross(m_inner ? (pointOnCurve - m_centrePoint).normalized : (m_centrePoint - pointOnCurve).normalized, Vector3.up);
    }

}

[System.Serializable]
public class BiarcCurveData
{
    public CurveSegmentData m_curveSegOne;
    public CurveSegmentData m_curveSegTwo;

    public CurveData _curveDataOne;
    public CurveData _curveDataTwo;

    private Vector3 _pointOne;
    private Vector3 _vectorOne;
    private Vector3 _pointTwo;
    private Vector3 _vectorTwo;
    public Vector3 _midPoint;

    public float _length;
    public float _rotation;

    public Vector3 PointOne
    {
        get
        {
            return _pointOne;
        }
        set
        {
            UpdateCurveData();
            _pointOne = value;
        }
    }

    public Vector3 VectorOne
    {
        get
        {
            return _vectorOne;
        }
        set
        {
            UpdateCurveData();
            _vectorOne = value;
        }
    }

    public Vector3 PointTwo
    {
        get
        {
            return _pointTwo;
        }
        set
        {
            UpdateCurveData();
            _pointTwo = value;
        }
    }

    public Vector3 VectorTwo
    {
        get
        {
            return _vectorTwo;
        }
        set
        {
            UpdateCurveData();
            _vectorTwo = value;
        }
    }

    private void UpdateCurveData()
    {
        _midPoint = MathC.FindBiarcPoint(PointOne, VectorOne, PointTwo, VectorTwo, 0.5f);

        var vertPoint1 = new Vector3(0, 0, PointOne.y);
        var vertVector1 = new Vector3(Vector3.Distance(PointOne, PointOne + VectorOne), 0, VectorOne.y);
        var vertPoint2 = new Vector3(0, 0, PointTwo.y);
        var vertVector2 = new Vector3(Vector3.Distance(PointTwo, PointTwo + VectorTwo), 0, -VectorTwo.y);
        _midPoint.y = MathC.FindBiarcPoint(vertPoint1, vertVector1, vertPoint2, vertVector2, 0.5f).z;

        MathC.FindPointOnArc(out m_curveSegOne, PointOne, VectorOne, _midPoint, 0, 0);
        MathC.FindPointOnArc(out m_curveSegTwo, _midPoint, -m_curveSegOne.m_vectorTwo, PointTwo, 0, 0);

        _length = m_curveSegOne.m_arcLength + m_curveSegTwo.m_arcLength;

        _curveDataOne = new CurveData(m_curveSegOne.m_pointOne, m_curveSegOne.m_vectorOne, m_curveSegOne.m_pointTwo);
        _curveDataTwo = new CurveData(m_curveSegTwo.m_pointOne, m_curveSegTwo.m_vectorOne, m_curveSegTwo.m_pointTwo);
    }

    public BiarcCurveData(Vector3 a_pointOne, Vector3 a_vectorOne, Vector3 a_pointTwo, Vector3 a_vectorTwo)
    {
        PointOne = a_pointOne;
        VectorOne = a_vectorOne;
        PointTwo = a_pointTwo;
        VectorTwo = a_vectorTwo;

        UpdateCurveData();
    }


    public Vector3 GetPointOnCurve(float a_time, float a_offset = 0f)
    {
        var returnVector = Vector3.zero;

        if (a_time < 0.5f)
        {
            returnVector = MathC.FindPointOnArc(m_curveSegOne, Mathf.InverseLerp(0, 0.5f, a_time), a_offset);
        }
        else
        {
            returnVector = MathC.FindPointOnArc(m_curveSegTwo, Mathf.InverseLerp(0.5f, 1, a_time), a_offset);
        }

        return returnVector;
    }

    public Vector3 GetPointOnCurveNormalized(float a_time, float a_offset = 0f, float a_angle = 0f)
    {
        var returnVector = Vector3.zero;

        var lengthOne = m_curveSegOne.m_arcLength;
        var lengthTwo = m_curveSegTwo.m_arcLength;

        var tLengthAlong = Mathf.Lerp(0, _length, a_time);

        var vectorOnPoint = Vector3.forward;
        var vectorOnPointNoOffset = Vector3.forward;

        var curveUsed = new CurveSegmentData();

        if (lengthOne >= tLengthAlong)
        {
            var normalisedT = Mathf.InverseLerp(0, lengthOne, tLengthAlong);
            returnVector = MathC.FindPointOnArc(m_curveSegOne, normalisedT, a_offset);
            vectorOnPointNoOffset = MathC.FindPointOnArc(m_curveSegOne, normalisedT, 0);
            vectorOnPoint = (m_curveSegOne.m_centrePoint - vectorOnPointNoOffset).normalized;
            curveUsed = m_curveSegOne;
            //returnVector.y =pointOnArc.z; 
        }
        else
        {
            var normalisedT = Mathf.InverseLerp(lengthOne, _length, tLengthAlong);
            returnVector = MathC.FindPointOnArc(m_curveSegTwo, normalisedT, a_offset);
            vectorOnPointNoOffset = MathC.FindPointOnArc(m_curveSegTwo, normalisedT, 0);
            vectorOnPoint = (m_curveSegTwo.m_centrePoint - vectorOnPointNoOffset).normalized;
            curveUsed = m_curveSegTwo;
            //returnVector.y =  pointOnArc.z; 
        }

        vectorOnPoint = Quaternion.Euler(0, -90, 0) * vectorOnPoint;

        if (curveUsed.m_inner)
        {
            a_angle = -a_angle;
        }



        var normalToPoint = (returnVector - vectorOnPointNoOffset);
        returnVector = vectorOnPointNoOffset + (Quaternion.AngleAxis(a_angle, vectorOnPoint) * normalToPoint);

        //Debug.DrawRay(returnVector, (Quaternion.AngleAxis(a_angle, vectorOnPoint) * returnVector).normalized);
        returnVector.y += MathC.CalculateCubicBezierPoint(PointOne, PointOne + (VectorOne * 2), PointTwo - (VectorTwo * 2), PointTwo, a_time).y;

        //returnVector.y += Vector3.Lerp(PointOne, PointTwo, a_time).y;


        return returnVector;
    }

    public Vector3 GetNormalOnCurveNormalized(float a_time, float a_angle = 0f)
    {
        var returnVector = Vector3.zero;

        var lengthOne = m_curveSegOne.m_arcLength;
        var lengthTwo = m_curveSegTwo.m_arcLength;

        var tLengthAlong = Mathf.Lerp(0, _length, a_time);

        var vectorOnPoint = Vector3.forward;
        var vectorOnPointNoOffset = Vector3.forward;

        var curveUsed = new CurveSegmentData();

        if (lengthOne >= tLengthAlong)
        {
            var normalisedT = Mathf.InverseLerp(0, lengthOne, tLengthAlong);
            returnVector = MathC.FindPointOnArc(m_curveSegOne, normalisedT, 1);
            vectorOnPointNoOffset = MathC.FindPointOnArc(m_curveSegOne, normalisedT, 0);
            vectorOnPoint = (m_curveSegOne.m_centrePoint - vectorOnPointNoOffset).normalized;
            curveUsed = m_curveSegOne;
            //returnVector.y =pointOnArc.z; 
        }
        else
        {
            var normalisedT = Mathf.InverseLerp(lengthOne, _length, tLengthAlong);
            returnVector = MathC.FindPointOnArc(m_curveSegTwo, normalisedT, 1);
            vectorOnPointNoOffset = MathC.FindPointOnArc(m_curveSegTwo, normalisedT, 0);
            vectorOnPoint = (m_curveSegTwo.m_centrePoint - vectorOnPointNoOffset).normalized;
            curveUsed = m_curveSegTwo;
            //returnVector.y =  pointOnArc.z; 
        }

        vectorOnPoint = Quaternion.Euler(0, -90, 0) * vectorOnPoint;

        var angleOffset = 90;
        if (curveUsed.m_inner)
        {
            a_angle = -a_angle;
            angleOffset = -angleOffset;
        }

        var normalToPoint = (returnVector - vectorOnPointNoOffset);


        return -(Quaternion.AngleAxis(a_angle + angleOffset, vectorOnPoint) * normalToPoint);
    }

    public float GetAngleOnCurve(float a_time)
    {
        float returnAngle = 0;
        var lengthOne = m_curveSegOne.m_arcLength;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);
        if (lengthOne >= tLengthAlong)
        {
            if (_curveDataOne.m_curveSeg.m_angleToPointTwo > 0)
            {
                returnAngle = Vector3.SignedAngle(Vector3.forward, _curveDataOne.m_curveSeg.m_centrePoint - GetPointOnCurveNormalized(a_time), Vector3.up);
            }
            else
            {
                returnAngle = Vector3.SignedAngle(Vector3.forward, GetPointOnCurveNormalized(a_time) - _curveDataOne.m_curveSeg.m_centrePoint, Vector3.up);
            }
        }
        else
        {
            if (_curveDataTwo.m_curveSeg.m_angleToPointTwo > 0)
            {
                returnAngle = Vector3.SignedAngle(Vector3.forward, _curveDataTwo.m_curveSeg.m_centrePoint - GetPointOnCurveNormalized(a_time), Vector3.up);
            }
            else
            {
                returnAngle = Vector3.SignedAngle(Vector3.forward, GetPointOnCurveNormalized(a_time) - _curveDataTwo.m_curveSeg.m_centrePoint, Vector3.up);
            }

        }

        return returnAngle;
    }

    public Vector3 GetVectorToCentreOnCurve(float a_time)
    {
        Vector3 returnVector = Vector3.zero;
        var lengthOne = m_curveSegOne.m_arcLength;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);

        if (lengthOne >= tLengthAlong)
        {
            var curveCentrePoint = _curveDataOne.m_curveSeg.m_centrePoint;
            curveCentrePoint.y = GetPointOnCurveNormalized(a_time).y;
            if (_curveDataOne.m_curveSeg.m_angleToPointTwo > 0)
            {
                returnVector = curveCentrePoint - GetPointOnCurveNormalized(a_time);
            }
            else
            {
                returnVector = GetPointOnCurveNormalized(a_time) - curveCentrePoint;
            }
        }
        else
        {
            var curveCentrePoint = _curveDataTwo.m_curveSeg.m_centrePoint;
            curveCentrePoint.y = GetPointOnCurveNormalized(a_time).y;
            if (_curveDataTwo.m_curveSeg.m_angleToPointTwo > 0)
            {
                returnVector = curveCentrePoint - GetPointOnCurveNormalized(a_time);
            }
            else
            {
                returnVector = GetPointOnCurveNormalized(a_time) - curveCentrePoint;
            }

        }

        return returnVector;
    }

    public float GetRadiusOnCurve(float a_time)
    {
        float returnRadius = 0;
        var lengthOne = m_curveSegOne.m_arcLength;
        var lengthTwo = m_curveSegTwo.m_arcLength;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);
        if (lengthOne >= tLengthAlong)
        {
            returnRadius = m_curveSegOne.m_radius;
        }
        else
        {
            returnRadius = m_curveSegTwo.m_radius;
        }
        return returnRadius;
    }

    public float GetLengthOnCurve(float a_time)
    {
        float returnRadius = 0;
        var lengthOne = m_curveSegOne.m_arcLength;
        var lengthTwo = m_curveSegTwo.m_arcLength;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);
        if (lengthOne >= tLengthAlong)
        {
            returnRadius = m_curveSegOne.m_arcLength;
        }
        else
        {
            returnRadius = m_curveSegTwo.m_arcLength;
        }
        return returnRadius;
    }

    public bool IsOnCurve(Vector3 pos, float maxDistance, out BiarcCurvePoint outCurvePoint)
    {
        bool isOnCurve = false;
        BiarcCurvePoint biarcCurvePoint = new BiarcCurvePoint
        {
            _curveData = this
        };


        CurvePoint curvePoint;

        if (_curveDataOne.IsOnCurve(pos, maxDistance, out curvePoint))
        {
            isOnCurve = true;
            biarcCurvePoint._pointOnCurve = curvePoint._pointOnCurve;
            biarcCurvePoint._vectorOnCurve = curvePoint._vectorOnCurve;
            biarcCurvePoint._timeOnCurve = curvePoint._timeOnCurve * 0.5f;
        }
        else if (_curveDataTwo.IsOnCurve(pos, maxDistance, out curvePoint))
        {
            isOnCurve = true;
            biarcCurvePoint._pointOnCurve = curvePoint._pointOnCurve;
            biarcCurvePoint._vectorOnCurve = curvePoint._vectorOnCurve;
            biarcCurvePoint._timeOnCurve = 0.5f + (curvePoint._timeOnCurve * 0.5f);
        }
        //if(m_curveSegOne)


        outCurvePoint = biarcCurvePoint;
        return isOnCurve;
    }
}

//[System.Serializable]
[Serializable]
public class CurveData
{
    public CurveSegmentData m_curveSeg;

    public Vector3 _pointOne;
    public Vector3 _vectorOne;
    public Vector3 _pointTwo;
    public Vector3 _vectorTwo;
    public Vector3 _midPoint;

    public float _length;
    public float _rotation;

    public Vector3 PointOne
    {
        get
        {
            return _pointOne;
        }
        set
        {
            UpdateCurveData();
            _pointOne = value;
        }
    }

    public Vector3 VectorOne
    {
        get
        {
            return _vectorOne;
        }
        set
        {
            UpdateCurveData();
            _vectorOne = value;
        }
    }

    public Vector3 PointTwo
    {
        get
        {
            return _pointTwo;
        }
        set
        {
            UpdateCurveData();
            _pointTwo = value;
        }
    }

    public Vector3 VectorTwo
    {
        get
        {
            return _vectorTwo;
        }
    }

    public void UpdateCurveData(float offset = 0)
    {
        if (offset != 0)
        {
            MathC.FindPointOnArc(out m_curveSeg, PointOne, VectorOne, PointTwo, 0);
            var pointOne = GetPointOnCurveNormalized(0, offset);
            var pointTwo = GetPointOnCurveNormalized(1, offset);
            _pointOne = pointOne;
            _pointTwo = pointTwo;
        }

        MathC.FindPointOnArc(out m_curveSeg, PointOne, VectorOne, PointTwo, 0);


        _vectorTwo = m_curveSeg.m_vectorTwo;
        _vectorOne = m_curveSeg.m_vectorOne;
        _length = m_curveSeg.m_arcLength;
    }

    public CurveData(Vector3 a_pointOne, Vector3 a_vectorOne, Vector3 a_pointTwo, float offset = 0)
    {
        PointOne = a_pointOne;
        VectorOne = a_vectorOne;
        PointTwo = a_pointTwo;

        UpdateCurveData(offset);
    }

    public CurveData(CurveData curveData)
    {
        PointOne = curveData.PointOne;
        VectorOne = curveData.VectorOne;
        PointTwo = curveData.PointTwo;

        UpdateCurveData();
    }

    private void InternalConstruct(Vector3 a_pointOne, Vector3 a_vectorOne, Vector3 a_pointTwo)
    {
        PointOne = a_pointOne;
        VectorOne = a_vectorOne;
        PointTwo = a_pointTwo;

        UpdateCurveData();
    }

    public Vector3 GetPointOnCurveNormalized(float a_time, float a_offset = 0f, float a_angle = 0f)
    {
        var returnVector = Vector3.zero;

        var lengthOne = m_curveSeg.m_arcLength;

        var tLengthAlong = Mathf.Lerp(0, _length, a_time);


        var vectorOnPoint = Vector3.forward;
        var vectorOnPointNoOffset = Vector3.forward;

        var curveUsed = new CurveSegmentData();

        var pointTwo = m_curveSeg.m_pointTwo;



        returnVector = MathC.FindPointOnArc(m_curveSeg, a_time, a_offset);
        vectorOnPointNoOffset = MathC.FindPointOnArc(m_curveSeg, a_time, 0);
        vectorOnPoint = (m_curveSeg.m_centrePoint - vectorOnPointNoOffset).normalized;
        curveUsed = m_curveSeg;

        vectorOnPoint = Quaternion.Euler(0, -90, 0) * vectorOnPoint;


        if (curveUsed.m_inner)
        {
            a_angle = -a_angle;
        }


        var normalToPoint = (returnVector - vectorOnPointNoOffset);
        returnVector = vectorOnPointNoOffset + (Quaternion.AngleAxis(a_angle, vectorOnPoint) * normalToPoint);

        returnVector.y += MathC.CalculateCubicBezierPoint(PointOne, PointOne + (VectorOne * 2), PointTwo - (VectorTwo * 2), PointTwo, a_time).y;

        return returnVector;
    }


    public Vector3 GetNormalOnCurveNormalized(float a_time)
    {
        var pointOnCurve = GetPointOnCurveNormalized(a_time);
        return Vector3.Cross(m_curveSeg.m_inner ? (pointOnCurve - m_curveSeg.m_centrePoint).normalized : (m_curveSeg.m_centrePoint - pointOnCurve).normalized, Vector3.up);
    }

    public float GetAngleOnCurve(float a_time)
    {
        float returnAngle = 0;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);
        returnAngle = m_curveSeg.m_angleToPointTwo;
        return returnAngle;
    }

    public float GetRadiusOnCurve(float a_time)
    {
        float returnRadius = 0;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);
        returnRadius = m_curveSeg.m_radius;
        return returnRadius;
    }

    public float GetLengthOnCurve(float a_time)
    {
        float returnRadius = 0;
        var tLengthAlong = Mathf.Lerp(0, _length, a_time);
        returnRadius = m_curveSeg.m_arcLength;
        return returnRadius;
    }

    public bool IsOnCurve(Vector3 pos, float maxDistance, out CurvePoint outCurvePoint)
    {
        bool isOnCurve = false;
        CurvePoint curvePoint = new CurvePoint();
        curvePoint._curveData = this;

        m_curveSeg.m_centrePoint.y = m_curveSeg.m_pointOne.y;

        if (m_curveSeg.m_angleToPointTwo < 0)
        {
            var angle = MathC.AngleHalf((m_curveSeg.m_pointOne - m_curveSeg.m_centrePoint).normalized, (pos - m_curveSeg.m_centrePoint).normalized, Vector3.down);
            if (Mathf.Abs(Vector3.Distance(m_curveSeg.m_centrePoint, pos) - m_curveSeg.m_radius) < maxDistance &&
            angle > 0 && angle < m_curveSeg.m_angleToTwoFromCentre)
            {
                curvePoint._timeOnCurve = Mathf.InverseLerp(0, m_curveSeg.m_angleToTwoFromCentre, angle);
                curvePoint._pointOnCurve = m_curveSeg.GetPositionOnCurve(curvePoint._timeOnCurve);
                isOnCurve = true;

                curvePoint._vectorOnCurve = (pos - curvePoint._pointOnCurve).normalized;
            }
        }
        else
        {
            var angle = MathC.AngleHalf((m_curveSeg.m_pointOne - m_curveSeg.m_centrePoint).normalized, (pos - m_curveSeg.m_centrePoint).normalized, Vector3.up);
            if (Mathf.Abs(Vector3.Distance(m_curveSeg.m_centrePoint, pos) - m_curveSeg.m_radius) < maxDistance &&
            angle > 0 && angle < m_curveSeg.m_angleToTwoFromCentre)
            {
                curvePoint._timeOnCurve = Mathf.InverseLerp(0, m_curveSeg.m_angleToTwoFromCentre, angle);
                curvePoint._pointOnCurve = m_curveSeg.GetPositionOnCurve(curvePoint._timeOnCurve);
                isOnCurve = true;

                curvePoint._vectorOnCurve = (pos - curvePoint._pointOnCurve).normalized;
            }
        }
        outCurvePoint = curvePoint;
        return isOnCurve;
    }

    public float ConvertLengthToTime(float length)
    {
        return MathC.InverseLerpUnclamped(0f, _length, length);
    }

    public float ConvertTimeToLength(float time)
    {
        return Mathf.LerpUnclamped(0f, _length, time);
    }

    public CurveData Reverse()
    {
        return new CurveData(PointTwo, VectorTwo, PointOne);
    }

    public bool IntersectionPoint(CurveData intersectingCurve, out CurvePoint curvePoint)
    {
        var centrePoint12D = new Vector2(m_curveSeg.m_centrePoint.x, m_curveSeg.m_centrePoint.z);
        var centrePoint22D = new Vector2(intersectingCurve.m_curveSeg.m_centrePoint.x, intersectingCurve.m_curveSeg.m_centrePoint.z);

        var iPoint1 = Vector2.zero;
        var iPoint2 = Vector2.zero;
        if (MathC.GetCirclIntersectionPoints(centrePoint12D, m_curveSeg.m_radius, centrePoint22D, intersectingCurve.m_curveSeg.m_radius, out iPoint1, out iPoint2))
        {
            curvePoint = new CurvePoint();
            var point1 = new Vector3(iPoint1.x, 0, iPoint1.y);
            var point2 = new Vector3(iPoint2.x, 0, iPoint2.y);

            if (IsOnCurve(point1, 0.1f, out curvePoint) && intersectingCurve.IsOnCurve(point1, 0.1f, out curvePoint))
            {
                return true;
            }
            else if (IsOnCurve(point2, 0.1f, out curvePoint) && intersectingCurve.IsOnCurve(point2, 0.1f, out curvePoint))
            {

                return true;
            }
            else
            {
                curvePoint = null;
                return false;
            }
        }
        else
        {
            curvePoint = null;
            return false;
        }
    }
}

[System.Serializable]
public class BiarcCurvePoint
{
    public BiarcCurveData _curveData;
    public Vector3 _pointOnCurve;
    public Vector3 _vectorOnCurve;
    public float _timeOnCurve;
}

[System.Serializable]
public class CurvePoint
{
    public CurveData _curveData;
    public Vector3 _pointOnCurve;
    public Vector3 _pointOnCurveCentre;
    public Vector3 _vectorOnCurve;
    public Vector3 _vectorOnCurveToPoint;
    public float _timeOnCurve;
    public float _curveWidth;
}