﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PseudoPhysics
{
    public class Simulation : MonoBehaviour
    {
        public float Density = 1;
        public Mass[] Masses = new Mass[0];                       


        public virtual void Init()                  
        {
            for (int a = 0; a < Masses.Length; ++a)   
                Masses[a].Init();                   
        }

        public virtual void Solve()                 
        {
            
        }

        public virtual void Simulate(float dt)      
        {
            for (int a = 0; a < Masses.Length; ++a)   
                Masses[a].Simulate(dt);             
        }

        public virtual void Operate(float dt)       
        {
            Init();                          
            Solve();                         
            Simulate(dt);                    
        }
    }
}
