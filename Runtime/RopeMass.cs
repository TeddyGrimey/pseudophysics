﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PseudoPhysics
{
    public class Mass
    {
        public float M;              
        public Vector3 Pos;          
        public Vector3 Vel;          
        public Vector3 Force;
        public Vector3 Forward;
        public RopeConstraint Constraint;
        public bool HasConstraint = false;

        public bool Collided = false;

        public Mass(float m)         
        {
            M = m;
        }

        public void ApplyForce(Vector3 force)
        {
            Force.x += force.x;
            Force.y += force.y;
            Force.z += force.z;
            //Force = new Vector3(Force.x + force.x, Force.y + force.y, Force.z + force.z);        
        }

        public void Init()                       
        {
            Force.x = 0;
            Force.y = 0;
            Force.z = 0;
        }

        public void Simulate(float dt)
        {
            if (HasConstraint && Constraint.Static)
            {
                var rigid = Constraint.Transform.GetComponent<Rigidbody>();
                if (rigid != null)
                {
                    rigid.AddForce((Force) * dt * 2, ForceMode.Force);

                    Vector3 targetDelta = Forward;
                    float angleDiff = Vector3.Angle(rigid.transform.forward, targetDelta);
                    Vector3 cross = Vector3.Cross(rigid.transform.forward, targetDelta);
                    rigid.AddTorque(cross * angleDiff * dt  * 2f, ForceMode.Force);
                    Vel += rigid.velocity * 0.5f * dt;
                    return;
                }
            }

            if (!HasConstraint || !Constraint.Static)
            {
                Vel.x += (Force.x / M) * dt;
                Vel.y += (Force.y / M) * dt;
                Vel.z += (Force.z / M) * dt;

                Pos.x += Vel.x * dt;
                Pos.y += Vel.y * dt;
                Pos.z += Vel.z * dt;
            }
            
        }
    }
}
