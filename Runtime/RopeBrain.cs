﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;
using System.Linq;

namespace PseudoPhysics
{ 
    public class RopeBrain : Simulation
    { 
        public RopeConstraint[] Constraints = new RopeConstraint[0];

        public float SpringMass = 1;
        public float SpringConstant = 1;
        public float RigidConstant = 1;
        public float SpringLength = 0;
        public float SpringFrictionConstant = 1;

        public Vector3 Gravitation;                   

        public float SpringRadius = 0.1f;

        public float GroundRepulsionConstant;         
        public float ColliderFrictionConstant;          
                                                      
        public float GroundAbsorptionConstant;        
        public float GroundHeight;                    
                                                      
        public float AirFrictionConstant;

        public Collider[] Colliders;

        [Space(10)]
        public Material Material;
        public Color Color;
        public float Width;

        private Spring[] _springs = new Spring[0];
        private ColliderInfo[] _colliderInfos = new ColliderInfo[0];
        private float _segmentDistance;

        private MaterialPropertyBlock _propBlock;

        private Vector3 _forceToAdd;

        private class ColliderInfo
        {
            public Collider Collider;
            public Transform Transform;
            public Vector3 Position;
            public Vector3 LocalScale;
            public Vector3 Up;
            public void UpdateInfo()
            {
                Transform = Collider.transform;
                Position = Transform.position;
                LocalScale = Transform.localScale;
                Up = Transform.up;
            }
        }

        void Start()
        {

            var tempMassesList = new List<Mass>();

            for (int i = 0; i < Constraints.Length - 1; i++)
            {
                var count = Mathf.RoundToInt(Vector3.Distance(Constraints[i].Transform.position, Constraints[i + 1].Transform.position) * Density);

                if(Constraints[i].Offsets.Count == 0)
                {
                    for (int t = 0; t < count; t++)
                    {
                        var newMass = new Mass(SpringMass);
                        newMass.Pos = Vector3.Lerp(Constraints[i].Transform.position, Constraints[i + 1].Transform.position, Mathf.InverseLerp(0, count - 1, t));

                        if (t == 0)
                        {
                            newMass.Constraint = Constraints[i];
                            newMass.HasConstraint = true;
                        }
                        if (t == count - 1)
                        {
                            newMass.Constraint = Constraints[i + 1];
                            newMass.HasConstraint = true;
                        }
                        tempMassesList.Add(newMass);
                    }
                }
                else
                {
                    var startPosition = Vector3.zero;
                    var endPosition = Vector3.zero;

                    for (int o = 0; o < Constraints[i].Offsets.Count + 1; o++)
                    {
                        if (o == 0)
                        {
                            startPosition = Constraints[i].Transform.position;
                            endPosition = Constraints[i].Offsets[o];
                        }
                        else if (o == Constraints[i].Offsets.Count)
                        {
                            startPosition = Constraints[i].Offsets[o - 1];
                            endPosition = Constraints[i + 1].Transform.position;
                        }
                        else
                        {
                            startPosition = Constraints[i].Offsets[o - 1];
                            endPosition = Constraints[i].Offsets[o];
                        }

                        count = Mathf.RoundToInt(Vector3.Distance(startPosition, endPosition) * Density);

                        for (int t = 0; t < count; t++)
                        {
                            var newMass = new Mass(SpringMass);
                            newMass.Pos = Vector3.Lerp(startPosition, endPosition, Mathf.InverseLerp(0, count - 1, t));
                            if (t == 0 && o == 0)
                            {
                                newMass.Constraint = Constraints[i];
                                newMass.HasConstraint = true;
                            }
                            if (t == count - 1 && o == Constraints[i].Offsets.Count)
                            {
                                newMass.Constraint = Constraints[i + 1];
                                newMass.HasConstraint = true;
                            }
                            tempMassesList.Add(newMass);
                        }
                    }
                    
                }

                
            }
            Masses = tempMassesList.ToArray();

            SpringLength = Vector3.Distance(Masses[0].Pos, Masses[1].Pos) * 0.5f;

            _springs = new Spring[Masses.Length - 1];

            var avsSpringDistance = 0f;
            for (int i = 0; i < Masses.Length - 1; ++i)
            {
                avsSpringDistance += Vector3.Distance(Masses[i].Pos, Masses[i + 1].Pos);
            }
            avsSpringDistance /= Masses.Length - 1;

            for (int i = 0; i < Masses.Length - 1; ++i)              
            {
                //var springDistance = Vector3.Distance(Masses[i].Pos, Masses[i + 1].Pos)* 0.8f;
                _springs[i] = new Spring(Masses[i], Masses[i + 1],
                    SpringConstant, RigidConstant, avsSpringDistance, SpringFrictionConstant);
            }
            
            _propBlock = new MaterialPropertyBlock();

            _colliderInfos = new ColliderInfo[Colliders.Length];
            for (int i = 0; i < _colliderInfos.Length; i++)
            {
                _colliderInfos[i] = new ColliderInfo();
                _colliderInfos[i].Collider = Colliders[i];
                _colliderInfos[i].Transform = Colliders[i].transform;
            }

            OnValidate();
        }

        private void OnValidate()
        {
            if (!Application.isPlaying || Masses == null) return;
            for (int i = 0; i < _springs.Length - 1; ++i)
            {
                _springs[i].SpringConstant = SpringConstant;
                _springs[i].RigidConstant = RigidConstant;
                _springs[i].FrictionConstant = SpringFrictionConstant;
            }

            for (int i = 0; i < Masses.Length; i++) 
            {
                if(Masses[i].HasConstraint)
                    Masses[i].M = Masses[i].Constraint.Mass;
                else
                    Masses[i].M = SpringMass;
            }
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < _colliderInfos.Length; i++)
            {
                _colliderInfos[i].UpdateInfo();
            }

            RopeUpdate();
            
        }

        private void LateUpdate()
        {
            _propBlock.SetVectorArray("_MassArray", Masses.ToList().ConvertAll(new System.Converter<Mass, Vector4>(MassToPos)));
            _propBlock.SetFloat("_Radius", Width);
            _propBlock.SetColor("_Color", Color);
            Draw();
        }


        void RopeUpdate()
        {
            float dt = Time.fixedDeltaTime;        

            float maxPossible_dt = 0.002f;    
                                              

            int numOfIterations = (int)(dt / maxPossible_dt) + 1;              
            if (numOfIterations != 0)                           
                dt = dt / numOfIterations;

            for (int a = 0; a < numOfIterations; ++a)         
                base.Operate(dt);
        }

        public void Draw()
        {
            var centre = Vector3.zero;
            var maxDist = 0f;
            for (int i = 0; i < Constraints.Length; i++)
            {
                centre += Constraints[i].Transform.position;
            }
            centre /= Constraints.Length;
            for (int i = 0; i < Constraints.Length; i++)
            {
                var dist = Vector3.Distance(centre, Constraints[i].Transform.position);
                if (dist > maxDist)
                {
                    maxDist = dist;
                }
            }


            var bounds = new Bounds(centre, Vector3.one * maxDist);

            
            
            Graphics.DrawProcedural(Material, bounds, MeshTopology.Triangles, (12 * Masses.Length) - 12, 1, null, _propBlock);
        }

        public static Vector4 MassToPos(Mass mass)
        {
            return mass.Pos;
        }

        public override void Solve()                           
        {
            base.Solve();
            for (int a = 0; a < Masses.Length - 1; ++a)         
            {
                if(a == 0)
                {
                    _springs[a].Solve(Masses[0].Constraint.Transform.up);  
                }        
                else
                {
                    _springs[a].Solve((Masses[a].Pos - Masses[a - 1].Pos).normalized);  
                }
            }

            for (int a = 0; a < Masses.Length; ++a)
            {
                _forceToAdd.x = Gravitation.x * Masses[a].M;
                _forceToAdd.y = Gravitation.y * Masses[a].M;
                _forceToAdd.z = Gravitation.z * Masses[a].M;

                _forceToAdd.x += -Masses[a].Vel.x * AirFrictionConstant;
                _forceToAdd.y += -Masses[a].Vel.y * AirFrictionConstant;
                _forceToAdd.z += -Masses[a].Vel.z * AirFrictionConstant;

                Masses[a].Force.x += _forceToAdd.x;
                Masses[a].Force.y += _forceToAdd.y;
                Masses[a].Force.z += _forceToAdd.z;

                if(a < Masses.Length - 2)
                {
                    Masses[a].Forward = (Masses[a + 1].Pos - Masses[a].Pos).normalized;
                }
                else
                {
                    Masses[a].Forward = (Masses[a].Pos - Masses[a - 1].Pos).normalized;
                }

                //Masses[a].ApplyForce(_forceToAdd);
                bool collided = false;
                for (int t = 0; t < 1; t++)
                {
                    for (int i = 0; i < Colliders.Length; i++)
                    {
                        if (Colliders[i] is SphereCollider sphereCollider)
                        {
                            if (Vector3.Distance(Masses[a].Pos, sphereCollider.transform.position) < (sphereCollider.radius * (Mathf.Max(_colliderInfos[i].LocalScale.z, Mathf.Max(_colliderInfos[i].LocalScale.x, _colliderInfos[i].LocalScale.y)))) + SpringRadius)
                            {
                                Masses[a].ApplyForce(-Masses[a].Vel * ColliderFrictionConstant);

                                Vector3 force = (Masses[a].Pos - sphereCollider.transform.position).normalized *
                                    (SpringRadius + (sphereCollider.radius * (Mathf.Max(_colliderInfos[i].LocalScale.z, Mathf.Max(_colliderInfos[i].LocalScale.x, _colliderInfos[i].LocalScale.y)))));

                                Masses[a].Vel = Masses[a].Vel - Vector3.Project(Masses[a].Vel, force);
                                Masses[a].Pos = sphereCollider.transform.position + force;
                                collided = true;
                            }
                        }
                        else if (Colliders[i] is CapsuleCollider capsuleCollider)
                        {
                            var height = capsuleCollider.height * _colliderInfos[i].LocalScale.y;
                            var radius = capsuleCollider.radius * Mathf.Max(_colliderInfos[i].LocalScale.x, _colliderInfos[i].LocalScale.z);
                            var ap = new Vector3(_colliderInfos[i].Position.x + (_colliderInfos[i].Up.x * ((height * 0.5f) - radius)),
                                                 _colliderInfos[i].Position.y + (_colliderInfos[i].Up.y * ((height * 0.5f) - radius)),
                                                 _colliderInfos[i].Position.z + (_colliderInfos[i].Up.z * ((height * 0.5f) - radius)));
                            var bp = new Vector3(_colliderInfos[i].Position.x - (_colliderInfos[i].Up.x * ((height * 0.5f) - radius)),
                                                 _colliderInfos[i].Position.y - (_colliderInfos[i].Up.y * ((height * 0.5f) - radius)),
                                                 _colliderInfos[i].Position.z - (_colliderInfos[i].Up.z * ((height * 0.5f) - radius))); 
                            Vector3 pa = Masses[a].Pos - ap, ba = bp - ap;
                            
                            float h = Mathf.Clamp(Vector3.Dot(pa, ba) / Vector3.Dot(ba, ba), 0.0f, 1.0f);
                            float dist = Vector3.Magnitude(pa - ba * h);

                            if (dist < radius + SpringRadius)
                            {
                                Masses[a].ApplyForce(-Masses[a].Vel * ColliderFrictionConstant);
                                
                                Vector3 force = (pa - ba * h).normalized *
                                    (SpringRadius + (radius));

                                Masses[a].Vel = Masses[a].Vel - Vector3.Project(Masses[a].Vel, force);
                                Masses[a].Pos = ap + (ba * h) + force;
                                collided = true;
                            }
                        }
                        else if(Colliders[i] is BoxCollider boxCollider)
                        {
                            var positionClosestToBox = boxCollider.ClosestPoint(Masses[a].Pos);
                            if (positionClosestToBox == Masses[a].Pos)
                            {
                                var transformedPosition = _colliderInfos[i].Transform.InverseTransformPoint(Masses[a].Pos);

                                var closestDist = Mathf.Infinity;
                                var pointU = ProjectPointOnPlane(Vector3.up, Vector3.up * 0.5f, transformedPosition);
                                var pointD = ProjectPointOnPlane(Vector3.down, Vector3.down * 0.5f, transformedPosition);
                                var pointL = ProjectPointOnPlane(Vector3.left, Vector3.left * 0.5f, transformedPosition);
                                var pointR = ProjectPointOnPlane(Vector3.right, Vector3.right * 0.5f, transformedPosition);
                                var pointF = ProjectPointOnPlane(Vector3.forward, Vector3.forward * 0.5f, transformedPosition);
                                var pointB = ProjectPointOnPlane(Vector3.back, Vector3.back * 0.5f, transformedPosition);
                                var dist = Vector3.Distance(transformedPosition, pointU);
                                if (dist < closestDist)
                                {
                                    closestDist = dist;
                                    positionClosestToBox = pointU;
                                }
                                dist = Vector3.Distance(transformedPosition, pointD);
                                if (dist < closestDist)
                                {
                                    closestDist = dist;
                                    positionClosestToBox = pointD;
                                }
                                dist = Vector3.Distance(transformedPosition, pointL);
                                if (dist < closestDist)
                                {
                                    closestDist = dist;
                                    positionClosestToBox = pointL;
                                }
                                dist = Vector3.Distance(transformedPosition, pointR);
                                if (dist < closestDist)
                                {
                                    closestDist = dist;
                                    positionClosestToBox = pointR;
                                }
                                dist = Vector3.Distance(transformedPosition, pointF);
                                if (dist < closestDist)
                                {
                                    closestDist = dist;
                                    positionClosestToBox = pointF;
                                }
                                dist = Vector3.Distance(transformedPosition, pointB);
                                if (dist < closestDist)
                                {
                                    closestDist = dist;
                                    positionClosestToBox = pointB;
                                }
                                positionClosestToBox = _colliderInfos[i].Transform.TransformPoint(positionClosestToBox);
                                var normalToBox = -(positionClosestToBox - Masses[a].Pos).normalized;
                                Masses[a].ApplyForce(-Masses[a].Vel * ColliderFrictionConstant);
                                Masses[a].Vel = Masses[a].Vel - Vector3.Project(Masses[a].Vel, (normalToBox * SpringRadius));
                                Masses[a].Pos = positionClosestToBox - (normalToBox * SpringRadius);
                                collided = true;
                            }
                            else
                            {
                                var normalToBox = (positionClosestToBox - Masses[a].Pos).normalized;
                                if (Vector3.Distance(Masses[a].Pos, positionClosestToBox) < SpringRadius)
                                {
                                    Masses[a].ApplyForce(-Masses[a].Vel * ColliderFrictionConstant);
                                    Masses[a].Vel = Masses[a].Vel - Vector3.Project(Masses[a].Vel, (normalToBox * SpringRadius));
                                    Masses[a].Pos = positionClosestToBox - (normalToBox * SpringRadius);
                                }
                                collided = true;
                            }
                        }


                    }
                }
                Masses[a].Collided = collided;

                if (Masses[a].HasConstraint) 
                {
                    if(Masses[a].Constraint.Static)
                    {
                        Masses[a].Pos = Masses[a].Constraint.Transform.position;
                        //Masses[a].Vel = Vector3.zero;
                    }
                    else
                    {
                        Masses[a].Constraint.Transform.position = Masses[a].Pos;
                    }
                    
                }


            }
        }

        public static Vector3 ProjectPointOnPlane(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {
            float distance = SignedDistancePlanePoint(planeNormal, planePoint, point) * -1;
            Vector3 translationVector = SetVectorLength(planeNormal, distance);
            return point + translationVector;
        }
        public static float SignedDistancePlanePoint(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {
            return Vector3.Dot(planeNormal, (point - planePoint));
        }
        public static Vector3 SetVectorLength(Vector3 vector, float size)
        {
            Vector3 vectorNormalized = Vector3.Normalize(vector);
            return vectorNormalized *= size;
        }

        public override void Simulate(float dt)                                            
        {
            base.Simulate(dt);  
        }

        private void OnDrawGizmos()
        {
            for (int i = 0; i < Constraints.Length; i++)
            {
                if (Constraints[i].Transform == null) return;
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(Constraints[i].Transform.position, 0.2f);
            }

            if(!Application.isPlaying)
            {
                var constraintsLength = 0f;

                for (int i = 0; i < Constraints.Length - 1; i++)
                {
                    constraintsLength += Vector3.Distance(Constraints[i].Transform.position, Constraints[i + 1].Transform.position);
                }

                for (int i = 0; i < Constraints.Length - 1; i++)
                {
                    var count = Mathf.RoundToInt(Vector3.Distance(Constraints[i].Transform.position, Constraints[i + 1].Transform.position) * Density);

                    if (Constraints[i].Offsets.Count == 0)
                    {
                        for (int t = 0; t < count - 1; t++)
                        {

                            var posA = Vector3.Lerp(Constraints[i].Transform.position, Constraints[i + 1].Transform.position, Mathf.InverseLerp(0, count - 1, t));
                            var posB = Vector3.Lerp(Constraints[i].Transform.position, Constraints[i + 1].Transform.position, Mathf.InverseLerp(0, count - 1, t + 1));
                            Gizmos.DrawLine(posA, posB);
                        }
                    }
                    else
                    {
                        var startPosition = Vector3.zero;
                        var endPosition = Vector3.zero;

                        for (int o = 0; o < Constraints[i].Offsets.Count + 1; o++)
                        {
                            if (o == 0)
                            {
                                startPosition = Constraints[i].Transform.position;
                                endPosition = Constraints[i].Offsets[o];
                            }
                            else if (o == Constraints[i].Offsets.Count)
                            {
                                startPosition = Constraints[i].Offsets[o - 1];
                                endPosition = Constraints[i + 1].Transform.position;
                            }
                            else
                            {
                                startPosition = Constraints[i].Offsets[o - 1];
                                endPosition = Constraints[i].Offsets[o];
                            }

                            count = Mathf.RoundToInt(Vector3.Distance(startPosition, endPosition) * Density);

                            for (int t = 0; t < count - 1; t++)
                            {
                                var posA = Vector3.Lerp(startPosition, endPosition, Mathf.InverseLerp(0, count - 1, t));
                                var posB = Vector3.Lerp(startPosition, endPosition, Mathf.InverseLerp(0, count - 1, t + 1));
                                Gizmos.DrawLine(posA, posB);
                            }
                        }

                    }
                    Gizmos.color = Color.green;
                }
                if (Masses == null) return;

                Gizmos.color = Color.cyan;

                for (int i = 0; i < Masses.Length; i++)
                {
                    Gizmos.DrawWireSphere(Masses[i].Pos, 0.05f);
                }
            }

            

        }

        public void UpdateAnimatorLimiter()
        {
            throw new System.NotImplementedException();
        }
    }
}
