﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace PseudoPhysics
{
    public class Spring              
    {                                       
        public Mass Mass1;                  
        public Mass Mass2;                  

        public float SpringConstant;     
        public float RigidConstant;           
        public float SpringLength;          
        public float FrictionConstant;

        private Vector3 _springVector;
        private float _r;
        private Vector3 _force;
        private float _springConstant;
        private Vector3 _forceToAdd;

        public Spring(Mass mass1, Mass mass2,
            float springConstant, float rigidConstant, float springLength, float frictionConstant)
        {
            SpringConstant = springConstant;
            SpringLength = springLength;
            FrictionConstant = frictionConstant;
            RigidConstant = rigidConstant;

            Mass1 = mass1;
            Mass2 = mass2;
        }

        public void Solve(Vector3 forward)
        {
            
            var forwardPos = Mass1.Pos + (forward * SpringLength);

            _springVector.x = ((Mass1.Pos.x - Mass2.Pos.x));
            _springVector.y = ((Mass1.Pos.y - Mass2.Pos.y));
            _springVector.z = ((Mass1.Pos.z - Mass2.Pos.z));

            Debug.DrawLine(Mass1.Pos, forwardPos, Color.magenta);
            Debug.DrawRay(Mass1.Pos, -_springVector, Color.green);

            //_springVector = Vector3.Slerp(_springVector, -forward * SpringLength,0.5f);

            _r = Mathf.Sqrt(_springVector.x * _springVector.x + _springVector.y * _springVector.y + _springVector.z * _springVector.z);

            _springConstant = (_r - SpringLength) * SpringConstant;

            _forceToAdd.x = (Mass1.Vel.x - Mass2.Vel.x) * -FrictionConstant;
            _forceToAdd.y = (Mass1.Vel.y - Mass2.Vel.y) * -FrictionConstant;
            _forceToAdd.z = (Mass1.Vel.z - Mass2.Vel.z) * -FrictionConstant;

            _force = _forceToAdd;

            _forceToAdd.x = (_springVector.x / -_r) * _springConstant;
            _forceToAdd.y = (_springVector.y / -_r) * _springConstant;
            _forceToAdd.z = (_springVector.z / -_r) * _springConstant;

            if (_r != 0)
            {
                _force.x += _forceToAdd.x;
                _force.y += _forceToAdd.y;
                _force.z += _forceToAdd.z;
            }

            Mass1.Force.x += _force.x;
            Mass1.Force.y += _force.y;
            Mass1.Force.z += _force.z;

            Mass2.Force.x += -_force.x;
            Mass2.Force.y += -_force.y;
            Mass2.Force.z += -_force.z;
        }

        //Union used by InvSqrt
        [StructLayout(LayoutKind.Explicit)]
        struct FloatIntUnion
        {
            [FieldOffset(0)]
            public float x;

            [FieldOffset(0)]
            public int i;
        }

        FloatIntUnion union = new FloatIntUnion();

        //Fast inverse Sqrt
        float InvSqrt(float x)
        {
            union.x = x;
            union.i = 0x5f3759df - (union.i >> 1);
            x = union.x;
            x = x * (1.5f - 0.5f * x * x * x);
            return x;
        }
    }
}
